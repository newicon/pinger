var program = require('commander');
var updateNotifier = require('update-notifier');
var pkg = require('../package.json');
// Check for newer version of Pinger
var notifier = updateNotifier({pkg: pkg});
if (notifier.update && levels.info >= loglevel) {
    notifier.notify();
}

var pinger = require('../pinger.js');

program
	.version(pkg.version)
	.usage('[command|options]');
 
program
	.command('test')
	.description('Run all ping web tests')
	.action(function(){
		pinger.test();
	});

program.parse(process.argv);

// display help if no commands given to nibu
if (!program.args.length) {
    program.help();
}