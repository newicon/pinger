/**
 * This script is nasty someone needs to rebuild it!
 */
var url = require('url'),
	http = require('http'),
	request = require('request'),
	mysql = require('mysql'),
	postmark = require('postmark')('15fa02db-fa13-47cc-853b-e4489f4fb18b'),
	util = require('util'),
	_ = require('underscore')
	async = require('async')

require('date-utils');

var config = require('config');

var dbConfig = {
  host     : config.db.host,
  user     : config.db.user,
  password : config.db.password
}

var dbTable = config.db.name+'.'+config.db.ping_table;

// parse the script "do" test script into a json array
// format:
// [{
//    cmd: {fun:'goto','args'{}},
//    tests:[{fun:'testFunctionName', args:'test arguments'},{fun:'testFunctionName', args:'test arguments'}]
// }] 
var parseScript = function(script){
	doScript = [];
	// parse script
    var lines = script.match(/^.*((\r\n|\n|\r)|$)/gm);
	for (l=0; l<=lines.length; l++) {

	    var line = lines[l];
	    if (line == '' || line == undefined) continue;

	    // search for goto command
	    var m = line.match(/(goto) (.*)/)
	    if(m != null) {if (m.length > 2 && m[1] == 'goto'){
	    	// found goto line
	    	// look for following test commands

	    	var scriptAction = {
				cmd:{
					fun:'goto', 
					args:{
						uri:url.parse(m[2].replace(/\s/g, "X")), 
						port:80,
						followRedirect:true,
						path:'/'
					}
				},
				tests:[]
			};
	    	doScript.push(scriptAction);
	    	continue;                
	    }}
	    
	    // search for sub goto commands
	    var m = lines[l].match(/(i should see) (.*)/)
	    if(m != null) {if(m.length > 2 && m[1] == 'i should see'){
            scriptAction.tests.push({fun:'shouldSee', args:m[2]});
	    }}
	
	}
	return doScript;
}

// define test functions
var testFunctions = {
	shouldSee : function(text, error, reponse, body){
		var res = (body.indexOf(text) != -1);
		return {
			log: 'should see: "' + text + '" - ' + (res ? 'passed' : 'failed'),
			result: res
		}
	}
};

// when something goes wrong we need to notify peeps
var notifyFail = function(row){
	postmark.send({
		"From": "theteam@newicon.net", 
		"To": "steve@newicon.net", 
		"Subject": "Pinger Test Failed: " + row.name,
		"Tag":row.name,
		"TextBody": "The following pinger script failed: \n\n " + row.name + ':\n'+row.script+'\n\nLog:\n'+row.last_log,
		"HtmlBody":"The following pinger script failed: <br /><br /> " + row.name + ':<br />'+row.script.replace(/\n/g, '<br />')+'<br/><br/>Log:<br/>'+row.last_log.replace(/\n/g, '<br />')
	});
}

// when something goes wrong we need to notify peeps
var notifySuccess= function(row){
	postmark.send({
		"From": "theteam@newicon.net", 
		"To": "steve@newicon.net", 
		"Subject": "Pinger Test Passed: " + row.name,
		"Tag":row.name,
		"TextBody": "This pinger test has started working again!! \n\nThe following pinger script passed: \n\n " + row.name + ':\n'+row.script+'\n\nLog:\n'+row.last_log,
		"HtmlBody":"This pinger test has started working again!! <br/><br/>The following pinger script passed: <br /><br /> " + row.name + ':<br />'+row.script.replace(/\n/g, '<br />')+'<br/><br/>Log:<br/>'+row.last_log.replace(/\n/g, '<br />')
	});
}

var logHttpError = function(error, response){
	var log = '';
	if (error || _.isUndefined(response)) {
		log = error;
	} else {
		var codeMsg = http.STATUS_CODES[response.statusCode] || '';
		log = 'with response code: ' + response.statusCode + ' - ' + codeMsg;
	}
	return 'failed ' + log;
}


var ticker = 0;
var requestTick = 0;

// ran for each row
var runtest = function(row, txt, callback) {
	txt = txt || '';
	// variable to store log messages
	var log = '';
	var doScript = parseScript(row.script)
	var testResult = false;
	_.each(doScript, function(scriptAction){
		
		if (scriptAction.cmd.fun == 'goto'){

			var startTime = new Date().getTime();
			request(scriptAction.cmd.args, function (error, response, body) {
				var responseTime = new Date().getTime() - startTime;

				requestTick += 1

				log += 'goto: ' + JSON.stringify(scriptAction.cmd.args.uri.host);

				testResult = (!error && response != undefined && response.statusCode == 200);

				log += (testResult ? ' - success' : ' - ' + logHttpError(error, response));

				_.each(scriptAction.tests, function(test){
					if (testResult == false) return;
					var ret = testFunctions[test.fun].call(this, test.args, error, response, body);
					testResult = ret.result;
					log += '\n' + ret.log + '';
				});

				log += "\nTeststatus: " + (testResult ? 'PASSED' : 'FAILED') + '\n';
	
				// should be able to use a previous db connection but this script stays running... so should I never bother
				// closing the connection ??
				var db = mysql.createConnection(dbConfig);
				db.connect();
				var update = {
					last_test_passed : testResult ? 1 : 0,
					last_tested      : new Date().toFormat('YYYY-MM-DD HH24:MI:SS'),
					last_log         : log,
					tested			 : row.tested += 1,
					response_time	 : responseTime
				};

				if (testResult && row.last_test_passed == 0) {
					// if the last test failed and the current test passed, we can send a notification to inform
					// everyone that the test is now working again
					notifySuccess(_.extend(row, update))
					update.fail_notice_sent = 0;
				}

				if (!testResult && row.fail_notice_sent == 0) {
					notifyFail(_.extend(row, update));
					update.fail_notice_sent = 1;
					update.fail_notice_sent_on = new Date().toFormat('YYYY-MM-DD HH24:MI:SS');
				}

				// insert a new ping row
				var insert = {
					test_id : row.id,
					date : new Date().toFormat('YYYY-MM-DD HH24:MI:SS'),
					address : scriptAction.cmd.args.uri.host,
					response_time : responseTime,
					response_code : (response != undefined) ? response.statusCode : 0,
					passed : testResult,
					error : (testResult ? '' : log) + (error ? error : '')
				}

				db.query('INSERT '+config.db.name+'.pinger_ping SET ?', insert, function(err, result){
					if(err) console.log(err);
				});
				db.query('UPDATE '+dbTable+' SET ? WHERE id = ' + row.id, update, function(err, result) {
					if (err) throw err;
				});
				db.end();
				
				console.log(log + ' requests: '+requestTick);
				if (callback)
					callback();
			});
		}
	});
}

var test = function(){
	console.log('TICK!!! - ' + new Date() + ' - ' + ticker);
	// create a new db connection for each tick.
	var connection = mysql.createConnection(dbConfig);
	connection.connect();

	connection.query('SELECT * from '+dbTable, function(err, rows, fields) {
		if (err) throw err;	
		
		var tests = [];
		_.each(rows, function(row, index){
			if(row.enabled) {
				tests.push(function(callback){
					runtest(row, ticker, callback);
				});
			}
		})
		
		async.series(
			tests,
			function(err, results){
				console.log('finished');
				setTimeout(function(){process.exit()},1000)
			}
		);
		
	});

	ticker += 1;
	connection.end();
}
module.exports = {test:test};
// setInterval(tick, 60000)
